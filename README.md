# Settlements

A settlement simmulation game.

# Compiling

With cmake, type the following on a shell:

```
cd build
cmake ../src
cmake --build .
```

If you use cmake-gui, set the following parameters:

- Where the sourcode is: `./src`
- Where to build the binaries: `./build`

Click on `Configure`, then click on `Generate`. Lastly, in a shell, enter the build directory and type `make`.

# License
GPLv3 (see COPYING.txt).
