/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   unit.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _UNIT_HPP
#define _UNIT_HPP 1

#include <string>
#include "position.hpp"

using namespace std;

namespace the_world{

  class World;  
  /**
   Abstract class that represent any unit.
  */
  class Unit {

  protected:
    position pos;

  public:
    
    /**
       Constructor
    */
    Unit(position pos);

    virtual string get_str();

    /**
     Move the unit in the given direction given the world rules.
     
     Given a direction, move the character une step towards that direction. Do
     it if and only if the world allows it (i.e. it can move there according
     to the world rules).

     @param dir One of the direction characters. Use one of `U D L R`
       characters for directions.
     @param world The world in which to move and where the rules applies.
     */
    bool move(char dir, World* world);
    
  };
}

#endif /* _UNIT_HPP */
