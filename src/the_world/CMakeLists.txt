add_library(the_world
  natural.cpp
  world.cpp
  player.cpp
  building.cpp
  settlement.cpp
  unit.cpp
  worker.cpp
  )

# set(THE_WORLD_SRCS
#   ${CMAKE_CURRENT_SOURCE_DIR}/natural.cpp
#   ${CMAKE_CURRENT_SOURCE_DIR}/world.cpp
#   PARENT_SCOPE)
