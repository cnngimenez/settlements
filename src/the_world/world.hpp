/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   world.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _WORLD_HPP
#define _WORLD_HPP 1

#include <string>
#include "position.hpp"
#include "natural.hpp"
#include "player.hpp"

/**
 Maximum amount of players. Please, add 1 for Gaia!

Player 0: Gaia
Player 1 to MAX_PLAYERS: Human/bots.
*/
#define MAX_PLAYERS 5

using namespace std;

namespace the_world{

  /**
   Represent a world and everything that exists in it...

   - Natural resources (the map)
   - Players and its units and buildings.

   # Rules
   This class will enforce the world rules:

   - No building should be built in a cell with another building.
   - No building should be built in a cell with a mountain or lake.
   - No unit should move into a cell with mountain or lake.
   - No unit can move into a cell with a wall building.
   - When a player end its game, all units and buildings are transfered to Gaia.
   - There are 20 instructions executed per turn per player (4 x 20 = 80).
  */
  class World {
   
    protected:

    /**
     Type of resources. For now these are the known resources:

     - Plains
     - Mountains
     - Desert
     - Lake
     */
    char resources[4] = {'P', 'M', 'D', 'L'};

    /**
     Resources that occupies space (buildings and units should not be here!)

     - Mountains
     - Lakes
    */
    char filling_resources[2] = {'M', 'L'};
    
    /**
     The natural part of the world.
    */
    Natural *natural;
    
    /**
     The Gaia player.

     This player will possess any unclaimed buildings, units and (future, not
     yet developed) monsters.
    */
    Player *gaia;
    
    public:

    Player *players[MAX_PLAYERS];
    
    /**
     Create a new world! :)
    */
    World();
    /**
     Destroy a world! :(
    */
    ~World();

    /**
     Fill the natural resources with random resources.
    */
    void fill_random();

    /**
     Is this position free of buildings?

     This does not considers the natural resource (mountains or lakes).

     @param pos The position of the cell.
     @see is_free()
    */
    bool is_free_of_buildings(position pos);
    
    /**
     Is this position free?

     Free means: no buildings, no mountains, no lakes.

     @param pos The position of the cell.
     @see is_free_of_buildings()
    */
    bool is_free(position pos);

    /**
     Return a string dump of the world.
     
     *This is for debugging purposes! Do not use!*

     @return {std::string} A string representation of the world.
    */
    string get_str();


    /**
     @name Unit and building orders
     
     The following methods are used to give orders to units and buildings.
    */
    ///@{
    

    /**
     Move the unit according to this world rules.

     If the unit find some obstacle, then return to the current position.
     
     It is supposed that a unit can move following these rules:

     - There is no natural resources like mountain or lake (filling_resources).
     - If the unit moves in a circular world.

     @param playernum The id/number of the player who possess the unit.
     @param unit_id The id of the player's unit.
     @para dir The direction where to move the unit. One of the character
       `U D L R`.
     @return true if the unit could be moved. False if there is some obstacle.
     */
    bool move_unit(uint playernum, uint unit_id, char dir);

    /**
     Calculate a new unit position considering this world rules.

     Return the new position if a unit with `pos` as current position makes a
     step toward the given direction.

     @param pos The current position.
     @param direction The direction where to move. One of the characters
       `UDLR`.
     @return The new position if it is free. The same position as `pos` if it is
       not allowed.
     */
    position calc_unit_move(position pos, char direction);
    
    ///@}
  };   

}

#endif /* _WORLD_HPP */

