/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   player.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "player.hpp"
#include "settlement.hpp"
#include "worker.hpp"

using namespace std;
using namespace the_world;

/**
   Constructor
 */
Player::Player(int number){
  this->number = number;
  available = false;
}

/**
   Destructor
 */
Player::~Player(){
}

void Player::set_settlement(position pos){
  buildings.push_back(new Settlement(pos));
}

void Player::start_game(position pos){
  buildings.push_back(new Settlement(pos));
  units.push_back(new Worker(pos));
}

bool Player::is_free(position pos){  
  for (vector<Building*>::iterator it = buildings.begin() ;
       it != buildings.end() ; ++it){
    if (!(*it)->in_position(pos)){
      return false;
    }
  }

  return true;
}

string Player::get_str(){
  string str;
  
  str += "Player Num.: ";
  str += to_string(number) + '\n';

  if (number == 0){
    str += "  I'm Gaia!\n";
  }

  str += "Buildings:\n";
  for (vector<Building*>::iterator it = buildings.begin() ;
       it != buildings.end(); ++it){
    str += (*it)->get_str() + '\n';
  }

  str += "Units:\n";
  for (vector<Unit*>::iterator it = units.begin() ;
       it != units.end(); ++it){
    str += (*it)->get_str() + '\n';
  }  
  
  return str;
}

bool Player::move_unit(uint unit_id, char dir, World* world){
  if (unit_id < units.size()){
    return units[unit_id]->move(dir, world);
  }else{
    return false;
  }
}
