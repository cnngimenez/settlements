/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   natural.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <random>
#include <functional>
#include "natural.hpp"

using namespace std;
using namespace the_world;

/**
   Constructor
 */
Natural::Natural() {
}

/**
   Destructor
 */
Natural::~Natural(){
}

void Natural::fill_random(char resources[], uint size) {
  char res;

  // Random generator with Uniform distribution.
  // This is C++11!
  std::default_random_engine generator;
  std::uniform_int_distribution<uint> distribution(0, size-1);
  auto dice = std::bind(distribution, generator);
  
  for (int row = 0; row < MAX_ROWS; row++){
    for (int col = 0; col < MAX_COLS; col++){

      res = resources[dice()];
      map[row][col] = res;

    }
  }
}

void Natural::fill(char resource){
  for (int row = 0; row < MAX_ROWS; row++){
    for (int col = 0; col < MAX_COLS; col++){
      map[row][col] = resource;
    }
  }
}

position Natural::random_space(){
  // Random generator with Uniform distribution.
  std::default_random_engine generator;

  std::uniform_int_distribution<uint> distr_cols(0, MAX_COLS-1);
  static auto dicecols = std::bind(distr_cols, generator);
  
  std::uniform_int_distribution<uint> distr_rows(0, MAX_ROWS-1);
  static auto dicerows = std::bind(distr_rows, generator);

  position ret;
  ret.row = dicerows();
  ret.col = dicecols();

  /**
   @todo Check if the position is a plain or dessert, else keep rolling.
  */
  
  return ret;
}

bool Natural::is_free(position pos, char resources[], uint size){
  char cell_resource = map[pos.row][pos.col];

  cout << "Natural::is_free:(" << pos.row << "," << pos.col
       << ")->|" << cell_resource << "|" << endl;
  
  for (uint i = 0; i < size; i++){
    if (cell_resource == resources[i]){
      return false;
    }
  }

  return true;
}

string Natural::get_str(){
  string str;

  for (int row = 0; row < MAX_ROWS; row++){
    for (int col = 0; col < MAX_COLS; col++){
      str += map[row][col];
    }
    str += '\n';
  }

  return str;
}

position Natural::calc_move(position pos, char direction){
  position newpos = pos;
  
  switch (direction){
  case 'U':
    if (newpos.row > 0){
      newpos.row -= 1;
    }else{
      newpos.row = MAX_ROWS - 1;
    }
    break;
  case 'D':
    if (newpos.row < MAX_ROWS){
      newpos.row += 1;
    }else{
      newpos.row = 0;
    }
    break;
  case 'L':
    if (newpos.col > 0){
      newpos.col -= 1;
    }else{
      newpos.col = MAX_COLS - 1;
    }
    break;
  case 'R':
    if (newpos.col < MAX_COLS){
      newpos.col += 1;
    }else{
      newpos.col = 0;
    }
    break;
  }
 
  return newpos;
}
