/* 
  Copyright 2020 poo
   
  Author: cnngimenez   

  player.hpp
   
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
   
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
   
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _PLAYER_HPP
#define _PLAYER_HPP 1

#include <string>
#include <vector>
#include "position.hpp"
#include "building.hpp"
#include "unit.hpp"

using namespace std;

namespace the_world{

  class World;
  
  /**
   A representation of the player and all its possessions.
  */
  class Player {
    
  protected:

    vector<Building*> buildings;
    vector<Unit*> units;

  public:
    /**
     Each player has a number, right?
    */
    int number;
    
    /**
     Is the player playing?
    */
    bool available;
        
    /**
     Create and initialize a player. 

     This will not set the starting position. Use set_settlement() for that.

     @param number {int} The player number.
    */
    Player(int number);
    ~Player();

    /**
     Set the starting possition.
    */
    void set_settlement(position pos);

    /**
     Set the starting position, the starting units and resources.

     @param pos A free position to start on. I won't check if it is free!!!
    */
    void start_game(position pos);

    /**
     Is this position free of buildings?
    */
    bool is_free(position pos);
    
    /**
     Return a string dump of the player.
     
     *This is for debugging purposes! Do not use!*

     @return {std::string} A string representation of the player.
    */
    string get_str();

    /**
     @name Unit and building orders
     Orders that this player can give to its units and buildings.
     */
    ///@{
    
    /**
     Move a unit according to the given world rules.

     @return true if the unit could be moved. False if the world rules don't
       allow the unit movement.
     */
    bool move_unit(uint unit_id, char dir, World* world);

    ///@}
    
  };   

}

#endif /* _PLAYER_HPP */

