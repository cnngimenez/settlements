/* 
  Copyright 2020 cnngimenez
   
  Author: cnngimenez   

  worker.hpp
   
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
   
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
   
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _WORKER_HPP
#define _WORKER_HPP 1

#include <string>
#include "position.hpp"
#include "unit.hpp"

using namespace std;

namespace the_world{


  class Worker : public Unit{
    
  public:
      
    /**
     Constructor
    */
    Worker(position pos);


    string get_str();

  };   

}

#endif /* _WORKER_HPP */

