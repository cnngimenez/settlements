/* 
   Copyright 2020 poo
   
   Author: cnngimenez   

   settlement.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SETTLEMENT_HPP
#define _SETTLEMENT_HPP 1

#include <string>
#include "position.hpp"
#include "building.hpp"

using namespace std;

namespace the_world{

  class Settlement : public Building{
    
    protected:
    private:
    public:
      
    /**
       Constructor
     */
    Settlement(position pos);
    ~Settlement();

    string get_str();

  };   

}

#endif /* _SETTLEMENT_HPP */

