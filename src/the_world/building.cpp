/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   building.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "building.hpp"

using namespace std;
using namespace the_world;

/**
   Constructor
 */
Building::Building(position pos) { 
  this->pos = pos;
}

bool Building::in_position(position pos){
  return this->pos.row == pos.row && this->pos.col == pos.col;
}

string Building::get_str(){
  string str;
  str += "Building:\nPosition: ";
  str += to_string(pos.row) + ", " + to_string(pos.col) + "\n";
  
  return str;
}
