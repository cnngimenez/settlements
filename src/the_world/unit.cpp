/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   unit.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "unit.hpp"
#include "world.hpp"

using namespace std;
using namespace the_world;

Unit::Unit(position pos){
  this->pos = pos;
}

string Unit::get_str(){
  string str = "Unit:\n";

  str += "Position: ";
  str += to_string(pos.row) + ", " + to_string(pos.col) + '\n';

  return str;
}

bool Unit::move(char dir, World* world){
  cout << "Unit moved ";
  position newpos = world->calc_unit_move(pos, dir);
  bool moved = newpos.row != pos.row || newpos.col != pos.col;

  cout << pos.row << "," << pos.col
       <<  "->" << newpos.row << "," << newpos.col << endl;
  
  pos = newpos;

  return moved;
}
