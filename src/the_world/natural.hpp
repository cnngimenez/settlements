/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   natural.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _NATURAL_HPP
#define _NATURAL_HPP 1

#include <string>
#include "position.hpp"

#define MAX_COLS 100
#define MAX_ROWS 100

using namespace std;

namespace the_world{

  class Natural {
    
    protected:
    
    /**
     A natural map is a simple character matrix. The character represents
     natural resources like desert, mountain, plain, lake, etc.
     */
    char map[MAX_ROWS][MAX_COLS];
    
    private:
    public:
      
    /**
       Constructor
     */
    Natural();
    ~Natural();

    /**
     Fill the map with random characters. Use the given ones only.

     Usage example:

     ```cpp
     natural->fill_random(['P', 'M', 'L', 'D'], 4);
     ```

     @param resources An array of characters to fill the map with.
     @param size The size of the resources array.
     */
    void fill_random(char resources[], uint size);
    
    /**
     Fill the map with the given resource.

     @param resource {char} The character to fill with.
    */
    void fill(char resource);

    /**
     Return a string dump of the world.
     
     *This is for debugging purposes! Do not use!*

     @return {std::string} A string representation of the world.
    */
    string get_str();

    position random_space();

    /**
     Is this place free?

     This means, is free of the given resources.

     @param pos The position to look for other resources.
     @param resources An array with the resources that occupies space.
     @param size The size of the resources array.
     */
    bool is_free(position pos, char resources[], uint size);

    /**
     Given a position (supposedly a current position) and a direction, calculate
     the next position step on that direction.

     In this world, coordinates are circular (i.e. if you make a step before 0,
     then you return to MAX_ROW/COL).

     @param pos The current position.
     @param direction The direction to where to move. One character of 
       `U D R L`.
     @return The new position according to the circular world.
     */
    position calc_move(position pos, char direction);
  };   

}

#endif /* _NATURAL_HPP */

