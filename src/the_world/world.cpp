/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   world.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <random>
#include <functional>
#include "world.hpp"

using namespace std;
using namespace the_world;

/**
   Constructor
    
   @todo Check that the building does not clash with others!
 */
World::World() {
  position pos;
  
  natural = new Natural();
  for (int i = 0; i < MAX_PLAYERS; i++){
    players[i] = new Player(i);

    pos = natural->random_space();
    players[i]->start_game(pos);
  }

  gaia = players[0];
}

World::~World(){
  ///@todo Write all the destructors and deletes here!
}

void World::fill_random() {
  natural->fill_random(resources, 4);  
}

bool World::is_free_of_buildings(position pos){
  bool it_is = true;

  for (int i = 0; i < MAX_PLAYERS; i++){
    it_is = it_is && players[i]->is_free(pos);
  }
  
  return it_is;
}

bool World::is_free(position pos) {
  return natural->is_free(pos, filling_resources, 2) &&
    is_free_of_buildings(pos);
}

string World::get_str(){
  string str;

  str += "Gaia:\n";
  str += natural->get_str();
  str += "Players:\n";
  for (int i = 0; i < MAX_PLAYERS; i++){
    str += players[i]->get_str();
    str += '\n';
  }
  
  return str;
}

bool World::move_unit(uint playernum, uint unit_id, char dir){
  cout << "World::move_unit:" << playernum << ","
       << unit_id << "," << dir << endl;
  
  if (playernum < MAX_PLAYERS){
    return players[playernum]->move_unit(unit_id, dir, this);
  }else{
    return false;
  }    
}

position World::calc_unit_move(position pos, char direction){
  position newpos = natural->calc_move(pos, direction);
  if (natural->is_free(newpos, filling_resources, 2)){
    return newpos;
  }else{
    return pos;
  }                                                   
}
