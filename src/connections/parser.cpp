/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   parser.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.hpp"

using namespace std;
using namespace connections;

Parser::Parser() {
}

Parser::~Parser(){
}

// Build* parse_build(string order, Player* player){}

Move* Parser::parse_move(string order, uint playernum){
  int unit_id = atoi(order.substr(1,3).c_str());
  char direction = order[4];

  return new Move(playernum, unit_id, direction);
}

// Gather* parse_gather(string order, Player* player){}
// Fight* parse_fight(string order, Player* player){}
// View* parse_view(string order, Player* player){}

Instruction* Parser::parse_order(string order, uint playernum){
  Instruction* instr = NULL;
  
  switch (order.at(0)){
  case 'B':
    // instr = parse_build(order, player);
    break;
  case 'M':
    instr = (Instruction*) parse_move(order, playernum);
    break;
  case 'G':
    // instr = parse_gather(order, player);
    break;
  case 'F':
    // instr = parse_fight(order, player);
    break;
  case 'V':
    // instr = parse_view(order, player);
    break;
  default:
    // Not recognized!
    instr = NULL;
  };

  return instr;
}

InstructionSet* Parser::parse_orders(string orders, uint playernum){
  // Split the string and parse each order
  string delimiter = "|";
  
  size_t pos = 0;
  std::string order;
  Instruction* instr = NULL;
  InstructionSet* instr_set = new InstructionSet();
  
  while ((pos = orders.find(delimiter)) != std::string::npos) {
    order = orders.substr(0, pos);

    instr = parse_order(order, playernum);
    instr_set->add(instr);
    
    orders.erase(0, pos + delimiter.length());
  }

  return instr_set;
}
