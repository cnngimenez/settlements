/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   pipe.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _PIPE_HPP
#define _PIPE_HPP 1

#include <iostream>
#include <fstream>
#include <string>
#include "../instructions/instruction_set.hpp"
#include "parser.hpp"

using namespace std;
using namespace instructions;

namespace connections {

  /**
   Creates and maintain the FIFO pipes for I/O with the client programs. Each
   player has got a Pipe instance associated to send and receive commands
   separately.

   FIFO pipes (or named pipes) are files used to communicate with this server.
   This class create two FIFO pipes because they are unidirectional (reading or
   writing only). 

   This class is used to consider this way of communication as one despite of
   using two "files".
  */
  class Pipe {

  protected:

    /**
     The associated player for this pipe.
    */
    uint playernum;
    /**
     Is this pipe active?
    */
    bool active;
    
    string pathout;
    string pathin;
    
    int fin;
    int fout;
    
  public:
    
    /**
     Create a new pipe.

     @param pathin The path to the input FIFO pipe filename.
     @param pathout The path to the output FIFO pipe filename.
     @param playernum The player number associated to this pipe.
    */
    Pipe(string pathin, string pathout, uint playernum);

    /**
     Destructor.
    */
    ~Pipe();

    /**
     Receive data. 

     It receives a string up to EOL or EOS.

     @return The string received.
    */
    string recv();

    /**
     Send data.

     @param data What to send. 
    */
    void send(string data);

    /**
     Get all orders from the pipe and parse it using the given Parser.
    */
    InstructionSet* parse_orders(Parser* parser);
    
  };

}
#endif /* _PIPE_HPP */
