/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   executor.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _EXECUTOR_HPP
#define _EXECUTOR_HPP 1

#include <string>
#include "../the_world/world.hpp"
#include "../instructions/instruction_set.hpp"

using namespace std;
using namespace the_world;
using namespace instructions;

namespace connections{


  class Executor {

  protected:
    World* world;
    
  public:
    
    /**
     Constructor
    */
    Executor(World* world);
    ~Executor();    
    
    string execute(InstructionSet* inst_set);
  };   

}

#endif /* _EXECUTOR_HPP */

