/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   pipe.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <iostream>
#include <sys/stat.h>
#include "pipe.hpp"

using namespace std;
using namespace connections;

Pipe::Pipe(string pathin, string pathout, uint playernum){
  this->pathin = pathin;
  this->pathout = pathout;
  this->playernum = playernum;
  active = false;

  mkfifo(pathin.c_str(), 0666);
  cout << "FIFO created: " << pathin << endl;
  mkfifo(pathout.c_str(), 0666);
  cout << "FIFO created: " << pathout << endl;
  
  fin = open(pathin.c_str(), O_RDONLY | O_NDELAY);
  cout << "Input pipe opened: " << pathin << endl;
}

Pipe::~Pipe(){
  close(fin);
  close(fout);
  
  unlink(pathin.c_str());
  unlink(pathout.c_str());
}

string Pipe::recv(){
  char buffer[400];
  int i = 0;
  ssize_t ret;
  char cur;
  bool cont = true;

  while (cont){
    ret = read(fin, &cur, 1);
    buffer[i] = cur;
    i++;
    cont = ret > 0 && i < 400;
  }
  buffer[i] = '\0';
  string line = buffer;
  
  if (line != "" && !active){
    cout << playernum << ": Data received. Opening output pipe." << endl;
    
    active = true;
    fout = open(pathout.c_str(), O_WRONLY | O_NDELAY);
  }
  
#ifdef DEBUG    
  cout << playernum << ": Received: ";
  cout << strlen(buffer) << ":"  << buffer << ":" << endl;
#endif
    
  return line;
}

void Pipe::send(string data){
  if (active){
#ifdef DEBUG
    cout << playernum  << ": Sending: " << data << "|" << endl;
#endif

    char output[400];
    strcpy(output, data.c_str());

    int ret = write(fout, output, strlen(output));
    
    if (ret < 0){
      close(fout);
      active = false;
      
      cout << "Player " << playernum
           << ": Problems on pipe, returned:" << ret << endl
           << "=> marking player as unavailable." << endl;
    }

  }
}

InstructionSet* Pipe::parse_orders(Parser* parser){
  string readed = recv();
  return parser->parse_orders(readed, playernum);
}
