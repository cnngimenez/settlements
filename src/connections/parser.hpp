/* 
   Copyright 2020 poo
   
   Author: cnngimenez   

   parser.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PARSER_HPP
#define _PARSER_HPP 1

#include <string>
#include "../instructions/instruction_set.hpp"
#include "../instructions/move.hpp"

using namespace instructions;

namespace connections{


  class Parser {
  public:
      
    /**
       Constructor
     */
    Parser();
    ~Parser();

    Instruction* parse_order(string order, uint playernum);
    
    InstructionSet* parse_orders(string orders, uint playernum);

  protected:
    /**
     Parse a move order.

     A move order should look like this: M001D

     @return a Move instance.
    */
    Move* parse_move(string order, uint playernum);
    
  };   

}

#endif /* _PARSER_HPP */

