/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   commander.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _COMMANDER_HPP
#define _COMMANDER_HPP 1

#include <vector>
#include <string>
#include "pipe.hpp"
#include "executor.hpp"
#include "parser.hpp"

using namespace std;

namespace connections{

  /**
   Manage the pipes and the instructions that comes from them.

   The parsing and execution of commands are delegated to the Parser and
   Executor instances respectively.
   */
  class Commander {

  protected:
    vector<Pipe*> pipes;

    Parser *parser;
    Executor *executor;
  public:
      
    /**
       Constructor
     */
    Commander(Executor* executor);
    ~Commander();

    /**
     Add a new pipe for the given player.
    */
    void add_pipe(Pipe* pipe);

    /**
     Notify all players(pipes) the beginning of the turn.
     */
    void new_turn();

    /**
     Notify all players(pipes) the end of the turn and process their commands.
     */
    void end_turn();
  };   

}

#endif /* _COMMANDER_HPP */

