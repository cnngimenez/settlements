/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   commander.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "commander.hpp"

using namespace std;
using namespace connections;

Commander::Commander(Executor* executor) {
  parser = new Parser;
  this->executor = executor;
}

Commander::~Commander(){
  delete(parser);
}

// void Commander::add_pipe(string pathin, string pathout, Player* player){
//   pipes->push_back(new Pipe(pathin, pathout, player);
// }

void Commander::add_pipe(Pipe* pipe){
  pipes.push_back(pipe);
}

void Commander::new_turn(){
    for (vector<Pipe*>::iterator it = pipes.begin(); it != pipes.end() ; ++it){
      (*it)->send("begin turn\n");
    }
}
void Commander::end_turn(){
  for (vector<Pipe*>::iterator it = pipes.begin(); it != pipes.end() ; ++it){
    (*it)->send("end turn\n");

    InstructionSet *inst_set = (*it)->parse_orders(parser);
    string answers = executor->execute(inst_set);

    (*it)->send(answers);
  }

  
}
