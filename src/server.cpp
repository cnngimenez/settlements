/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   server.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <thread>
#include "the_world/world.hpp"
#include "connections/commander.hpp"
#include "connections/pipe.hpp"
#include "connections/executor.hpp"

using namespace std;
using namespace the_world;
using namespace connections;

/**
 @todo Breaks abstraction and responsabilities! world->players[]!? really!?
 */
void create_pipes(Commander* comm){
  string prefix = "run/pipes/player_";

  cout << "Creating pipes" << endl;
  
  for (uint i = 0; i < 4; i++){
    string num = to_string(i);
    string pathin = prefix + num;
    string pathout = prefix + num;
    pathin += "-in.pipe";
    pathout += "-out.pipe";

    comm->add_pipe(new Pipe(pathin, pathout, i));

    cout << "Pipe created for player " << num << endl;
  }
}


int main(int argc, char** argv){
  uint dumpcounter = 0;
  
  World *world = new World();
  world->fill_random();

  string str = world->get_str();
  cout << str << endl;

  Commander* commander = new Commander(new Executor(world));
  create_pipes(commander);

  while (1){
    commander->new_turn();
    this_thread::sleep_for(chrono::seconds(1));
    commander->end_turn();

    dumpcounter++;

    if (dumpcounter = 500){
      // str = world->get_str();
      // cout << str << endl;
      dumpcounter = 0;
    }
  }
  
  return 0;
}
