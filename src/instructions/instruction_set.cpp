/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   instruction_set.cpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "instruction_set.hpp"

using namespace instructions;

/**
   Constructor
 */
InstructionSet::InstructionSet() {
}

/**
   Destructor
 */
InstructionSet::~InstructionSet(){
}

void InstructionSet::add(Instruction* instruction){
  if (instructions.size() < InstructionSet::max_instructions){
    instructions.push_back(instruction);
  }
}

string InstructionSet::run(World* world){
  string results = "";
  
  for (vector<Instruction*>::iterator it = instructions.begin();
       it != instructions.end() ; ++it){
    results += (*it)->run(world);
    results += "\n";
  }

  return results;
}
