/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   move.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MOVE_HPP
#define _MOVE_HPP 1

#include <string>
#include "instruction.hpp"
#include "../the_world/world.hpp"

using namespace the_world;

namespace instructions{

  class Move : public Instruction{
    static const char up = 'U';
    static const char down = 'D';
    static const char left = 'L';
    static const char right = 'R';

  protected:
    uint playernum;
    uint unit_id;

    /**
     Direction where to move to.
    */
    char dir;
    
  public:
        
    /**
       Constructor
     */
    Move(uint playernum, uint unit_id, char dir);

    string run(World* world);

  };   

}

#endif /* _MOVE_HPP */

