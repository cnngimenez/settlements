/* 
   Copyright 2020 cnngimenez
   
   Author: cnngimenez   

   instruction_set.hpp
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _INSTRUCTION_SET_HPP
#define _INSTRUCTION_SET_HPP 1

#include <vector>
#include <string>
#include "instruction.hpp"
#include "../the_world/world.hpp"

using namespace std;
using namespace the_world;

namespace instructions{

  /**
   A set of instructions.
  */ 
  class InstructionSet {

    /**
     Maximum amount of instructions that can be stored.
    */
    static const size_t max_instructions = 20;
    
    protected:

    vector<Instruction*> instructions;
    
    public:
      
    /**
       Constructor
     */
    InstructionSet();
    ~InstructionSet();

    /**
     Add an instruction.
    */
    void add(Instruction *instruction);

    /**
     Run all instructions in this set.

     @param world The world affected by the instructions.
     @return The results of executing all the instructions.
    */
    string run(World *world);
  };   

}

#endif /* _INSTRUCTION_SET_HPP */

